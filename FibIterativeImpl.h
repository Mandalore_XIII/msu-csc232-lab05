//
// Created by James Daehn on 9/26/16.
//

#ifndef MSU_CSC232_LAB05_FIBITERATIVEIMPL_H
#define MSU_CSC232_LAB05_FIBITERATIVEIMPL_H

#include "IFib.h"

class FibIterativeImpl  : public IFib {
public:
    virtual unsigned long getFibonacciNumber(const unsigned long &n) const override;
    virtual std::vector<unsigned long> getFibonacciSequence(const unsigned long &n) const override;
    virtual ~FibIterativeImpl() {}
};


#endif //MSU_CSC232_LAB05_FIBITERATIVEIMPL_H
