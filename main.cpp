#include <chrono>
#include <iomanip>
#include <iostream>
#include "IFib.h"
#include "FibRecursiveImpl.h"
#include "FibIterativeImpl.h"
#include "FibDynamicImpl.h"
#include "TowersOfHanoi.h"

using namespace std;

void setupCoutFractions(int);
double getCpuTimeForFib(IFib *fib, unsigned long n);
void printTable(unsigned long& begin, unsigned long& end, unsigned long& step);
double getCpuTimeForHanoi(unsigned numDisks);

int main(int argc, char *argv[]) {
    cout << "Welcome to Lab 5!" << endl << endl;
    unsigned long begin{0};
    unsigned long end{0};
    unsigned long step{1};
    bool tableBoundsRead{false};
    if (argc == 3) {
        begin = std::stoul(argv[1]);
        end = std::stoul(argv[2]);
        tableBoundsRead = true;
    }

    int response{8};
    do {
        cout << "\nSelect from the menu:\n\n";
        cout << "1. Find fib(n) using recursive implementation\n";
        cout << "2. Find fib(n) using iterative implementation\n";
        cout << "3. Find fib(n) using dynamic implementation\n";
        cout << "4. Print table in range given by command line arguments\n";
        cout << "5. Play Towers of Hanoi\n";
        cout << "6. See how many moves it takes to solve Towers of Hanoi puzzle.\n";
        cout << "7. Find execution times for Towers of Hanoi game\n";
        cout << "8. Quit\n";
        cout << "\n\t\tWhat do you want to do? ";
        cin >> response;

        unsigned long n{0};
        unsigned numDisks{0};
        IFib *fib { nullptr };
        TowersOfHanoi game;

        switch (response) {
            case 1:
                fib = new FibRecursiveImpl;
                cout << "\t\tEnter the value of n: ";
                cin >> n;
                cout << "\n\t\tfib(" << n << ") = " << fib->getFibonacciNumber(n) << endl;
                break;
            case 2:
                fib = new FibIterativeImpl;
                cout << "\t\tEnter the value of n: ";
                cin >> n;
                cout << "\n\t\tfib(" << n << ") = " << fib->getFibonacciNumber(n) << endl;
                break;
            case 3:
                fib = new FibDynamicImpl;
                cout << "\t\tEnter the value of n: ";
                cin >> n;
                cout << "\n\t\tfib(" << n << ") = " << fib->getFibonacciNumber(n) << endl;
                break;
            case 4:
                if (!tableBoundsRead) {
                    cout << "\n\t\tEnter lower bound for n: ";
                    cin >> begin;
                    cout << "\t\tEnter upper bound for n: ";
                    cin >> end;
                }
                printTable(begin, end, step);
                break;
            case 5:
                cout << "\t\tHow many disks? ";
                cin >> numDisks;
                game.solve(numDisks);
                break;
            case 6:
                cout << "\t\tHow many disks? ";
                cin >> numDisks;
                cout << "\t\tIt would take " << game.getNumMoves(numDisks) << " moves to solve the puzzle "
                        << "with " << numDisks << " disks." << endl;
                break;
            case 7:
                cout << "\t\tHow many disks? ";
                cin >> numDisks;
                cout << "\t\tIt takes " << getCpuTimeForHanoi(numDisks) << " seconds of execution to solve the puzzle "
                                                                        << "with " << numDisks << " disks." << endl;
                break;
            default:
                response = 8;
        }
        delete fib;
        fib = nullptr;
    } while (response != 8);

    return EXIT_SUCCESS;
}

void printTable(unsigned long& begin, unsigned long& end, unsigned long& step) {
    IFib *rFib = new FibRecursiveImpl;
    IFib *iFib = new FibIterativeImpl;
    IFib *dFib = new FibDynamicImpl;

    const string N_HEADING = "n";
    const string HEADING1 = "Recursive (s)";
    const string HEADING2 = "Iterative (s)";
    const string HEADING3 = "Dynamic (s)";

    const unsigned long TABLE_BEGIN = begin;
    const unsigned long TABLE_END = end;
    const unsigned long TABLE_STEP = 1;
    const int WIDTH = 15;
    const int DIGITS = 6;

    setupCoutFractions(DIGITS);
    cout << endl;
    cout << "-----------------------------------------------" << endl;
    cout << setw(2) << N_HEADING << setw(WIDTH) << HEADING1 << setw(WIDTH) << HEADING2 << setw(WIDTH) <<  HEADING3 << endl;
    cout << "-----------------------------------------------" << endl;
    unsigned long n{0};
    for (n = TABLE_BEGIN; n <= TABLE_END; n += TABLE_STEP) {
        cout << setw(2) << n;
        cout << setw(WIDTH) << getCpuTimeForFib(rFib, n);
        cout << setw(WIDTH) << getCpuTimeForFib(iFib, n);
        cout << setw(WIDTH) << getCpuTimeForFib(dFib, n) << endl;
    }

    delete rFib;
    delete iFib;
    delete dFib;
}

void setupCoutFractions(int fractionDigits) {
    cout.precision(fractionDigits);
    cout.setf(ios::fixed, ios::floatfield);
    if (fractionDigits == 0) {
        cout.unsetf(ios::showpoint);
    } else {
        cout.setf(ios::showpoint);
    }
}

double getCpuTimeForFib(IFib *fib, unsigned long n) {
    std::clock_t startcputime = std::clock();
    fib->getFibonacciNumber(n);
    return (std::clock() - startcputime) / (double) CLOCKS_PER_SEC;
}

double getCpuTimeForHanoi(unsigned numDisks) {
    TowersOfHanoi hanoi;
    std::clock_t startcputime = std::clock();
    hanoi.getNumMoves(numDisks);
    return (std::clock() - startcputime) / (double) CLOCKS_PER_SEC;
}